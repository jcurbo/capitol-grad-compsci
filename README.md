# Graduate work for Capitol College

James Curbo <jcurbo@pobox.com>

This is a collection of code I have written to support my graduate work in
computer science at [Capitol College](http://www.capitol-college.edu).  

## List of subprojects

### CS512 Computer Language Design
- C++ code implementing syntax and semantic analysis for a compiler for a simple C-like language.
- Various code written while working through the book "Seven Languages in Seven Weeks" by Bruce Tate, including Clojure, Haskell, and Prolog.  

### CS705 Distributed Programming
A few sample RPC implementations written in Python.

### IAE673 Secure Information Transfer and Storage

Some cipher solvers and helper tools mostly written in Python

* Caesar cipher
* Monoalphabetic cipher
* Vigenere cipher

## License for code
See LICENSE file in the repo.