#!/usr/bin/python

# Client for mortgage calculator
# CS705 HW3
# James Curbo <jcurbo@pobox.com>

import Pyro4

class CTier(object):
    """Class for client interaction with mortgage calculator

    @author: James Curbo
    @organization: Capitol College
    @contact: jcurbo@pobox.com
    """

    
    btier_uri = "PYRONAME:btier"
    """Pyro URI for business tier server"""

    btier = Pyro4.Proxy(btier_uri)
    """Class instance for business tier server"""

    def login(self):
        """
        login to business tier server
        """
    
        print "Please login to the system.\n"
        
        # execute login code once, then loop if we don't get a good username or
        # password
        condition = True
        while condition:
            username = raw_input("Enter your username: ")
            password = raw_input("Enter your password: ")
        
            # authenicate with business tier
            # check_creds returns True on failure, which will cause us to loop
            condition = self.btier.check_creds(username, password)
            if (condition):
                print "Wrong username or password, please try again.  Press Ctrl-C to exit.\n"
    
        print "Login successful.\n"
    
    def get_freq(self):
        """
        asks user for payment frequency
    
        @return: frequency of payments: m = monthly, b = biweekly
        @rtype: character
        """
    
        condition = True
        while condition:
    
            freq = raw_input("Do you want monthly or bi-weekly payments? Type m for monthly or b for bi-weekly: ")
            if not (freq == "m" or freq == "b"):
                print "Invalid input, please try again.\n"
            else:
                condition = False
    
        return freq
    
    def get_number(self):
        """asks user for input and verifies the input is a number
    
        Convenience function to ask for numerical input.  Will only accept numbers as
        input, will loop if something other than a number is entered.
    
        @return: a number
        @rtype: C{int}
        """
    
        condition = True
        while condition:
            try:
                num = float(raw_input())
                break
            except ValueError:
                # we get this exception if the user enters something other than a
                # number
                print "Invalid input, please try again. Press Ctrl-C to exit.\n"
    
        return num
    
    def get_principal(self):
        """
        asks user for amount of principal
    
        @return: amount of principal
        @rtype: C{int}
        """
    
        print "Please enter the principal (amount to be borrowed): "
        p = self.get_number()
        return p
    
    
    def get_period(self):
        """
        asks user for period of loan in years
    
        @return: period of loan, in years
        @rtype: C{int}
    
        """
    
        print "Please enter the period, in years (amount of time to pay back the loan): "
        y = self.get_number()
        return y

    def get_payment(self, principal, period, freq):
        """retrieves payment from business tier

        @param principal: amount of principal, in dollars
        @type principal: C{float}
        @param period: period of repayment, in years
        @type period: C{int}
        @param freq: freqency of repayment
        @type freq: character

        @return: payment amount, in dollars
        @rtype: C{float}
        """

        return self.btier.calc_payment(principal, period, freq)

    def get_interest_rate(self):
        """retrieves interest rate from business tier

        @return: interest rate percentage, per year
        @rtype: C{float}
        """

        return self.btier.get_interest_rate()
    
    
def main():
    """main function of client tier"""

    print "Welcome to the mortgage calculator.\n"

    # Create a new CTier instance
    client = CTier()

    client.login()

    principal = client.get_principal()
    period = client.get_period()
    freq = client.get_freq()

    payment = client.get_payment(principal, period, freq)

    print "\n"
    print "The principal amount is $%d." % principal
    print "The interest rate is %.0f percent per year." % (client.get_interest_rate() * 100)
    print "The repayment period is %d years." % period
    if freq == "m":
        print "Payments will be made monthly."
        print "Your monthly payment will be $%.2f." % payment
    elif freq == "b":
        print "Payments will be made bi-weekly."
        print "Your bi-weekly payment will be $%.2f." % payment


if __name__ == "__main__":
    main()





