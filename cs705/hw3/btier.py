#!/usr/bin/python

# Business tier server for mortgage calculator
# CS705 HW3
# James Curbo <jcurbo@pobox.com>

import Pyro4

class BTier(object):
    """Class for mortgage calculations

    @author: James Curbo
    @organization: Capitol College
    @contact: jcurbo@pobox.com
    """

    def check_creds(self, username, password):
        """checks credentials against database

        @param username: name of user
        @type username: string
        @param password: password of user
        @type password: string

        @return: False for success, True for failure
        @rtype: bool
        """

        # Pretend we access an authentication store in the database.
        # For our purposes we will only take username "james" and password
        # "test".
        # returns False on success
        # returns True on failure
        if username == 'james' and password == 'test':
            return False
        else:
            return True


    def get_interest_rate(self):
        """retrieves interest rate from database

        @return: interest rate, yearly
        @rtype: C{float}
        """

        # pretend we have a database call here.
        # We're just going to return a fixed interest rate.
        # Let's assume it's 5%.
        return 0.05

    def calc_payment(self, p, y, f):
        """calculates payment for mortgage

        @param p: principal, in dollars
        @type p: C{float}
        @param y: period for repayment, in years
        @type y: C{int}
        @param f: frequency of payment, monthly or biweekly
        @type f: character

        @return: amount of payment, in dollars
        @rtype: C{float}
        """

        # sets the interest rate and number of compounding periods
        if (f == "m"):
            # monthly payments
            i = self.get_interest_rate() / 12
            n = y * 12
        elif (f == "b"):
            # bi-weekly payments
            i = self.get_interest_rate() / 24
            n = y * 24

        # calculate monthly payment
        m = (p * (i * ((1+i)**n))) / (((1+i)**n) - 1)

        return m

def main():
    """main function of business tier"""
    btier = BTier()
    Pyro4.Daemon.serveSimple(
        {
            btier: "btier"
        },
        ns=True)

if __name__=="__main__":
    main()
