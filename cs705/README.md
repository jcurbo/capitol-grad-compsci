# Homework for CS 705

Multithreaded and Distributed Programming

Capitol College

James Curbo <james@curbo.org>

## Assignment

The assignment involved writing a mortgage calculator using a simple client/server architecture. The assignment had two parts:

* Write a client and business tier for the calculator.
* Create a mortgage class that would calculate monthly and bi-weekly payments. Indicate variables and methods needed.

## Implementation

To fulfill this assignment I have written a simple client/server app in Python.  As I have never used Python before this was a learning experience on several levels.

I used the Pyro RPC framework to handle the intracacies of implementing the RPC interactions between the client and business tiers.  It is an easy to use framework that did not add much complexity to the assignment. Pyro's homepage is at [http://pyro.sourceforge.net](http://pyro.sourceforge.net/)

I implemented the client and business tiers as Python objects, each in their own file.

## Program structure

This is a simple breakdown of the interaction of the tiers.

User <-> Client tier <-> Business tier <-> Database

The client tier is client.py, the business tier is btier.py.  We assume a database exists and simply return values as hardcoded for this exercise.

## Program flow

This is a simple runthrough of the program's flow.

* User asks for monthly payment rate
* User provides amount of mortgage (principal)
* User provides period of time for loan
* User provides preference for monthly or bi-weekly payments
* Client tier supplies all info to business tier
* Business tier queries database tier for interest rate
* Database tier returns interest rate to business tier
* Business tier calculates payment
* Business tier returns info to client tier
* Client tier displays info to user

## Formulas

This is the formula used for compound interest.

M = P (i(1+i)^n) / ((1+i)^n - 1)

* M = Payment
* P = principal (amount of money being borrowed)
* i = interest for each compounding period
* n = number of compounding periods

Source: [http://www.ifitbreaks.com/interest.htm](http://www.ifitbreaks.com/interest.htm)

## Running this program

Here are instructions for running this program, if desired.

* Install the Pyro framework.  One way is using "easy_install"
	* `easy_install Pyro4`
* Run the Pyro name server.  This is how Pyro clients and servers find each other.
	* `python -m Pyro4.naming`
* Run the business tier.  You will get a message that should say "Pyro daemon running"
	* `python btier.py`
* Run the client tier and input information as requested.
	* `python client.py`

## Further documentation

I have used the `epydoc` program to further document the code.  The output of the `epydoc` command is available in the `doc` subdirectory as HTML.
