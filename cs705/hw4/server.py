#!/usr/bin/python

# CS 705 HW 4
# James Curbo <jcurbo@pobox.com>
# Capitol College
# Multithreaded & Distributed Programming
#
# Server side code for HW4

import sys
import pickle
import SocketServer

class calc_engine:
    """ Class for the calculation engine.  Provides operator functions as well as the RPC server
    	side handler."""

# These are the operator functions
    def add(self, num1, num2):
        return num1 + num2

    def sub(self, num1, num2):
        return num1 - num2

    def mul(self, num1, num2):
        return num1 * num2

    def div(self, num1, num2):
        return num1 / num2


    def process(self, data):
	""" recieves message from client, processes and returns answer
	"""
	answer = 0

	# Pickle is a Python way of serializing/deserializing data.
	# Using it here to convert the recieved blob of into back into a dict.
	try:
	    msg = pickle.loads(data)
	except:
	    # Bail out at any error
	    print "Unexpected error:", sys.exc_info()[0]
	    raise


	# Decision for 'func' part of message, which tells us what function to call
	if msg['func'] == "add":
	    answer = self.add(msg['args'][0], msg['args'][1])
	elif msg['func'] == "sub":
	    answer = self.sub(msg['args'][0], msg['args'][1])
	elif msg['func'] == "mul":
	    answer = self.mul(msg['args'][0], msg['args'][1])
	elif msg['func'] == "div":
	    answer = self.div(msg['args'][0], msg['args'][1])

	return answer

class calc_udp_handler(SocketServer.BaseRequestHandler):
    """ This is how we handle incoming UDP connections.
    """

    def handle(self):
	data = self.request[0].strip()
	socket = self.request[1]

	# send data to calculator to process, get answer back
	engine = calc_engine()
	answer = str(engine.process(data))

	# send answer back to client
	socket.sendto(answer, self.client_address)

if __name__ == "__main__":
    HOST, PORT = "localhost", 9999
    server = SocketServer.UDPServer((HOST, PORT), calc_udp_handler)
    server.serve_forever()


