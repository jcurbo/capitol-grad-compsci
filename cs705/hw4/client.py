#!/usr/bin/python

# CS 705 HW 4
# James Curbo <jcurbo@pobox.com>
# Capitol College
# Multithreaded & Distributed Programming
#
# Client side code for HW4

import sys
import socket
import pickle

class calc_client:

    def core(self):
	""" Core client functionality.  Asks for input, parses it and calls client stubs
	as necessary."""

	print "Please enter an expression to be evaluated."
	print "This is a simple calcuator, so the expression must be in the form <num> <operator> <num>."
	print "<num> is any integer.  <operator> is one of +, -, *, /"

	# takes input from user and splits it into a list
	expr = raw_input("> ").split(' ')

	# Converting strings to ints
	expr[0] = int(expr[0])
	expr[2] = int(expr[2])

	# answer from our query
	answer = 0

	# make a decision based on the operator
	if expr[1] == '+':
	    answer = self.add(expr[0], expr[2])
	elif expr[1] == '-':
	    answer = self.sub(expr[0], expr[2])
	elif expr[1] == '*':
	    answer = self.mul(expr[0], expr[2])
	elif expr[1] == '/':
	    answer = self.div(expr[0], expr[2])
	else:
	    print "Error: Operator not valid: Exiting."
	    sys.exit()

	print "The answer is %s." % answer

	return

# Client stubs.  Before calling the invoke function, we build an argument list
# structure to pass to invoke().  
    def add(self, num1, num2):
	args = (num1, num2)
	return self.invoke("add", args)

    def sub(self, num1, num2):
	args = (num1, num2)
	return self.invoke("sub", args)

    def mul(self, num1, num2):
	args = (num1, num2)
	return self.invoke("mul", args)

    def div(self, num1, num2):
	args = (num1, num2)
	return self.invoke("div", args)


    def invoke(self, func, args):
	""" Client side of the RPC.  Handles marshalling and sending data over via UDP. """

	# we use a dict to store our data
	msg = dict()
	msg['func'] = func
	msg['args'] = args

	# Pickle serializes the data structure for sending over the wire
	pmsg = pickle.dumps(msg)

	# Send to the server
	HOST, PORT = "localhost", 9999
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	sock.sendto(pmsg, (HOST, PORT))

	# Receive answer back
	answer = sock.recv(1024)

	return answer


if __name__ == "__main__":

    # Not a lot to do here, core() handles everything.
    calc = calc_client()
    calc.core()







