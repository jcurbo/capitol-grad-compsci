// Capitol College CS512
// James Curbo <jcurbo@pobox.com>
//
// Project - Part 2

#include <iostream>
#include <cctype>
#include <iomanip>

using namespace std;

double convert_str2double(string str)
{
	double value = 0;
	bool flag_invalid = false;
	bool flag_decimal = false;
	int denom = 1;

	for (int i=0; i < str.length() && flag_invalid == false; i++) {
		if (str[i] == '.') {
			// if we see a decimal point, we switch on the decimal flag,
			// which puts all further digits into the decimal part of the
			// number
			flag_decimal = true;
		} else if (isdigit(str[i])) {
			int digit = str[i] - '0';
			if (flag_decimal) {
				value = value + (digit / (double)(10 * denom));
				denom *= 10;
			} else {
				value = 10 * value + digit;
			}
		} else {
			// not a digit, not a decimal point
			flag_invalid = true;
			value = -1;
		}
	}
	return value;
}

int main (int argc, char *argv[]) 
{ 	
	double result = 0;

 	if (argc > 1) {
 		for (int count = 1; count < argc; count++) {
 			result = convert_str2double(argv[count]);
 			if (result == -1) {
 				cout << "The input \"" << argv[count] << "\" has non-number characters "
 				     << "and thus could not be converted to a number." << endl;
 			} else {
 				cout << setprecision(15)  << "string " << argv[count] << ", float " << result << endl;
 			}
 		}
 	} else {
 		cout << "Converts strings that contain numbers to numerical representations." << endl;
 		cout << "Usage: " << argv[0] << " <string> <string> ..." << endl;
 	}

 	return 0;

}
