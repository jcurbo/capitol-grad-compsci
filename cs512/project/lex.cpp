// Capitol College CS512
// James Curbo <jcurbo@pobox.com>
// 
// Project Part 3: Lexical Analyzer

// For simplicity's sake, we will be collapsing the usual .h/.cpp class
// definition files into this file.

/*

BNF to be implemented

expr 	::= term [{+ | –} term]*
term 	::= factor [{* | /} factor]* 
factor 	::= (expr) | number 

terminals:
	number ::= digit+ | digit+ . digit* | digit* . digit+
	operator ::= + | - | * | / | ( | )
	digit ::= 0 .. 9

*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <cstdlib>

using namespace std;

class Token 
{
    public:
	//Token();
	//~Token();

	string symbol;
	string lexemeType;

	double semanticNumber;
	string semanticWord;
};

class LexicalAnalyzer
{
    public:
	LexicalAnalyzer(string expr);
	//~LexicalAnalyzer();

	bool get_token(Token* token);

    private:
	string _expr;
	string::iterator it;

};

LexicalAnalyzer::LexicalAnalyzer(string expr)
{
    _expr = expr;
    it = _expr.begin();
}

bool LexicalAnalyzer::get_token(Token* token)
{
    bool retval;

    typedef enum {
	ST_INITIAL,
	ST_1,
	ST_2,
	ST_3,
	ST_4,
	ST_5,
	ST_6,
	ST_FINAL,
	ST_FAIL
    } state_t;

    state_t currentState = ST_INITIAL;

    string lexemeType;

    // record current position of iterator, so we can use it to grab the
    // symbol at the end
    string::iterator begin = it;

    while (!(currentState == ST_FINAL || currentState == ST_FAIL) && it < _expr.end()) {
	if (currentState == ST_INITIAL) {
	    if (isdigit(*it))
		currentState = ST_1;
	    else if (*it == '.')
		currentState = ST_3;
	    else if (*it == '+' || *it == '-' || *it == '*' || *it == '/' || *it == '(' || *it == ')')
		currentState = ST_5;
	    else if (*it == ' ' || *it == '\n' || *it == '\t') {
		currentState = ST_INITIAL;
		begin = it+1;
	    } else
		currentState = ST_FAIL;
	} else if (currentState == ST_1) {
	    if (isdigit(*it))
		currentState == ST_1;
	    else if (*it == '.')
		    currentState = ST_2;
            else {
	        currentState = ST_FINAL;
	        lexemeType = "number";
	    }
	} else if (currentState == ST_2) {
	    if (isdigit(*it))
		currentState == ST_2;
	    else {
	        currentState = ST_FINAL;
	        lexemeType = "number";
	    }
	} else if (currentState == ST_3) {
	    if (isdigit(*it))
	        currentState = ST_4;
	    else
	        currentState = ST_FAIL;
	} else if (currentState == ST_4) {
	    if (isdigit(*it))
		currentState == ST_4;
	    else {
		currentState = ST_FINAL;
		lexemeType = "number";
            }
	} else if (currentState == ST_5) {
	    currentState = ST_FINAL;
	    lexemeType = "operator";
	}

	++it;
    }

    if (currentState == ST_FAIL) {
	cerr << "Lexical error" << endl;
	retval = false;
    } else if (currentState == ST_FINAL) {
	// retract
	--it;

	string tmp(begin, it);
	token->symbol = tmp;

	if (lexemeType == "number") {
	    token->semanticNumber = atof(token->symbol.c_str());
	} else if (lexemeType == "operator") {
	    if (token->symbol == "+") {
		token->semanticWord = "add";
	    } else if (token->symbol == "-") {
		token->semanticWord = "subtract";
	    } else if (token->symbol == "*") {
		token->semanticWord = "multiply";
	    } else if (token->symbol == "/") {
		token->semanticWord = "divide";
	    } else if (token->symbol == "(") {
		token->semanticWord = "lparen";
	    } else if (token->symbol == ")") {
		token->semanticWord = "rparen";
	    }
	}
	
	token->lexemeType = lexemeType;

	retval = true;
    } else if (it == _expr.end()) {
	cout << "reached end of input file" << endl;
	retval = false;
    } else {
	// any other state is a bug
	cerr << "shouldn't be here " << currentState << endl;
	retval = false;
    }

    return retval;
}	

void printTokens(vector<Token> tokenList)
{
	for (vector<Token>::iterator it = tokenList.begin(); it < tokenList.end(); ++it) {
	    cout << "token symbol: $" << it->symbol << "$" << endl;
	    cout << "      lexeme: " << it->lexemeType << endl;
	    cout << "      value: ";
	    if (it->lexemeType == "number")
		cout << it->semanticNumber << endl;
	    else
		cout << it->semanticWord << endl;
	}
}

int main(int argc, char *argv[])
{
    	int retval = 0;
	ifstream inputFile;
	stringstream buffer;

	if (argc > 1) {
	    inputFile.open(argv[1], ios::in);
	    if (inputFile.is_open()) {
		// pulls the whole file into the string
		buffer << inputFile.rdbuf();
		inputFile.close();
		
		LexicalAnalyzer *lex = new LexicalAnalyzer(buffer.str());

		vector<Token> tokenList;

		// This will eventually be where the syntax analyzer is
		bool lexflag = true;
		while(lexflag) {
		    Token *t = new Token;
		    lexflag = lex->get_token(t);
		    if (lexflag)
			tokenList.push_back(*t);
		}
		
		printTokens(tokenList);
	
	    } else {
		cerr << "Could not open file " << argv[1] << " for reading." << endl;
		retval = 1;
	    }

	} else {
	    cerr << "Performs lexical analysis on input file, converting it to tokens." << endl;
	    cerr << "Usage: " << argv[0] << " <filename>" << endl;
	    retval = 1;
	}

	return retval;
}




	
