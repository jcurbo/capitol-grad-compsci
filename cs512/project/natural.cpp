/* Capitol College CS512
 * James Curbo <jcurbo@pobox.com>
 *
 * Project - Part 1
 */

#include <iostream>
#include <cctype>

using namespace std;

/* Converts a string containing a natural number (non-negative integer) to an integer value.
 * Returns the value, or -1 if a non-digit character was encountered in the
 * string.
 */
 int convert_str2int(string str) 
 {
 	int value = 0;
 	bool flag_invalid = false;

 	for (int i = 0; i < str.length() && flag_invalid == false; i++) {
 		if (isdigit(str[i])) {
 			int digit = str[i] - '0';
 			value = 10 * value + digit;
 		} else {
 			// we've hit a nondigit character and need to abort
 			value = -1;
 			flag_invalid = true;
 		}
 	}
 	return value;
 }

float convert_str2float(string str)
{
	float value;
	bool flag_invalid = false;
	bool flag_decimal = false;
	int denom = 1;

	for (int i=0; i < str.length() && flag_invalid == false; i++) {
		if (str[i] == '.') {
			// if we see a decimal point, we switch on the decimal flag,
			// which pulls all further input into the decimal part of the
			// number
			flag_decimal = true;
		} else if (isdigit(str[i])) {
			int digit = str[i] - '0';
			if (flag_decimal) {
				value = value + (digit / (10 * denom));
				denom *= 10;
			} else {
				value = 10 * value + digit;
			}
		} else {
			// not a digit, not a decimal point
			flag_invalid = true;
		}
	}
	return value;
}






 int main (int argc, char *argv[]) 
 {
 	int result = 0;

 	if (argc > 1) {
 		for (int count = 1; count < argc; count++) {
 			result = convert_str2int(argv[count]);
 			if (result == -1) {
 				cout << "The input \"" << argv[count] << "\" has non-number characters "
 				     << "and thus could not be converted to a natural number." << endl;
 			} else {
 				cout << "string " << argv[count] << ", integer " << result << endl;
 			}
 		}
 	} else {
 		cout << "Converts strings that contain natural numbers to integers." << endl;
 		cout << "Usage: natural <string> <string> ... <string>" << endl;
 	}

 	return 0;

 }
