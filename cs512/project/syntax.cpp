// Capitol College CS512
// James Curbo <jcurbo@pobox.com>
// 
// Project Part 5: Semantic Analyzer

// For simplicity's sake, we will be collapsing the usual .h/.cpp class
// definition files into this file.

/*

BNF to be implemented

expr 	::= term [{+ | –} term]*
term 	::= factor [{* | /} factor]* 
factor 	::= (expr) | number 

terminals:
	number ::= digit+ | digit+ . digit* | digit* . digit+
	operator ::= + | - | * | / | ( | )
	digit ::= 0 .. 9

*/

/* Semantic Analyzer Notes
 * 
 * I implemented this as an interpreter, thus I did not create a seperate
 * SemanticAnalzyer class.  A perform_operation() function was added to the
 * SyntaxAnalyzer class, which uses a class member variable vector<double>
 * stack and an operator saved from the expr() and term() functions.  
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <cstdlib>

using namespace std;

class Token 
{
    public:

	string symbol;

	string lexemeType;
	double semanticNumber;
	string semanticWord;

	void print();
};

void Token::print()
{
    cout << "token symbol: $" << this->symbol << "$" << endl;
    cout << "      lexeme: " << this->lexemeType << endl;
    cout << "      value: ";
    if (this->lexemeType == "number")
	cout << this->semanticNumber << endl;
    else
	cout << this->semanticWord << endl;

}

class LexicalAnalyzer
{
    public:
	LexicalAnalyzer(string expr);

	bool get_token(Token* token);

    private:
	string _expr;
	string::iterator it;

};

LexicalAnalyzer::LexicalAnalyzer(string expr)
{
    _expr = expr;
    it = _expr.begin();
}

bool LexicalAnalyzer::get_token(Token* token)
{
    bool retval;

    enum state_t {
	ST_INITIAL,
	ST_1,
	ST_2,
	ST_3,
	ST_4,
	ST_5,
	ST_6,
	ST_FINAL,
	ST_FAIL,
	ST_END
    };

    state_t currentState = ST_INITIAL;

    string lexemeType;

    // record current position of iterator, so we can use it to grab the
    // symbol at the end
    string::iterator begin = it;

    while (!(currentState == ST_FINAL || currentState == ST_FAIL || currentState == ST_END)) {
	if (currentState == ST_INITIAL) {
	    if (isdigit(*it))
		currentState = ST_1;
	    else if (*it == '.')
		currentState = ST_3;
	    else if (*it == '+' || *it == '-' || *it == '*' || *it == '/' || *it == '(' || *it == ')')
		currentState = ST_5;
	    else if (*it == ' ' || *it == '\n' || *it == '\t') {
		currentState = ST_INITIAL;
		begin = it+1;
	    } else if (it == _expr.end()) 
		currentState = ST_END;
	    else
		currentState = ST_FAIL;
	} else if (currentState == ST_1) {
	    if (isdigit(*it))
		currentState == ST_1;
	    else if (*it == '.')
		    currentState = ST_2;
            else {
	        currentState = ST_FINAL;
	        lexemeType = "number";
	    }
	} else if (currentState == ST_2) {
	    if (isdigit(*it))
		currentState == ST_2;
	    else {
	        currentState = ST_FINAL;
	        lexemeType = "number";
	    }
	} else if (currentState == ST_3) {
	    if (isdigit(*it))
	        currentState = ST_4;
	    else
	        currentState = ST_FAIL;
	} else if (currentState == ST_4) {
	    if (isdigit(*it))
		currentState == ST_4;
	    else {
		currentState = ST_FINAL;
		lexemeType = "number";
            }
	} else if (currentState == ST_5) {
	    currentState = ST_FINAL;
	    lexemeType = "operator";
	}

	++it;
    }

    if (currentState == ST_FAIL) {
	cout << "Lexical error" << endl;
	retval = false;
    } else if (currentState == ST_FINAL) {
	// retract
	--it;

	string tmp(begin, it);
	token->symbol = tmp;

	if (lexemeType == "number") {
	    token->semanticNumber = atof(token->symbol.c_str());
	} else if (lexemeType == "operator") {
	    if (token->symbol == "+") {
		token->semanticWord = "add";
	    } else if (token->symbol == "-") {
		token->semanticWord = "subtract";
	    } else if (token->symbol == "*") {
		token->semanticWord = "multiply";
	    } else if (token->symbol == "/") {
		token->semanticWord = "divide";
	    } else if (token->symbol == "(") {
		token->semanticWord = "lparen";
	    } else if (token->symbol == ")") {
		token->semanticWord = "rparen";
	    }
	}
	
	token->lexemeType = lexemeType;

	//token->print();

	retval = true;
    } else if (currentState == ST_END) {
	//cout << "reached end of buffer" << endl;
	retval = false;
    } else {
	// any other state is a bug
	cout << "shouldn't be here " << currentState << endl;
	retval = false;
    }

    return retval;
}	

class SyntaxAnalyzer
{
    public:
	SyntaxAnalyzer()
	{
	    this->parseOnly = false;
	}
	
	double evaluate(string expression);

	bool expr(Token *token);
	bool term(Token *token);
	bool factor(Token *token);
	void clearStack();
	void perform_operation(char oper);

	// true = performs semantic analysis
	// false = only parses
	bool parseOnly;

    private:
	LexicalAnalyzer *lex;
	vector<double> stack;
	
};

double SyntaxAnalyzer::evaluate(string expression)
{
    this->lex = new LexicalAnalyzer(expression);

    bool lexflag = true;
    Token *t = new Token;

    // kick start the lexical analyzer
    lexflag = this->lex->get_token(t);

    while(lexflag) {
	lexflag = this->expr(t);
    }

    if (!this->parseOnly) {
        cout << "Final result of expression evaluation: "
    	<< this->stack.front() << endl;
    }

    this->clearStack();

    delete this->lex;

    // later, this will actually evaluate the value of the expression (once we
    // do semantic analysis), for now we just return 0 for success
    return 0;
}


void SyntaxAnalyzer::clearStack()
{
    this->stack.clear();
}

bool SyntaxAnalyzer::expr(Token *token)
{
    bool retval;
    char oper;
    if (!this->term(token)) {
	retval = false;
    } else {
	retval = true;
	while ((token->lexemeType == "operator") && ((token->symbol == "+") || (token->symbol == "-"))) {
	    oper = token->symbol[0];
	    if (!this->lex->get_token(token)) {
		retval = false;
	    } else if (!this->term(token))
		retval = false;
	    this->perform_operation(oper);
	}
    }
    return retval;
}

bool SyntaxAnalyzer::term(Token *token)
{
    bool retval;
    char oper;
    if (!factor(token)) {
	retval = false;
    } else {
	retval = true;
	while ((token->lexemeType == "operator") && ((token->symbol == "*") || (token->symbol == "/"))) {
	    oper = token->symbol[0];
	    if (!this->lex->get_token(token)) {
		retval = false;
	    } else if (!this->factor(token)) 
	    	retval = false;
	    this->perform_operation(oper);
	}
    }
    return retval;
}

bool SyntaxAnalyzer::factor(Token *token)
{
    bool retval;
    if ((token->lexemeType == "operator") && (token->symbol == "(")) {
	if (!this->lex->get_token(token)) {
	    retval = false;
	} else if (!this->expr(token)) {
	    retval = false;
	} 
	
	if ((token->lexemeType != "operator") || (token->symbol != ")")) {
	    cout << "Syntax error, mismatched parentheses" << endl;
	} else {
	    retval = true;
	}
    } else if (token->lexemeType !=  "number") {
	cout << "Syntax error, number expected" << endl;
	retval = false;
    } else {
	this->stack.push_back(token->semanticNumber);
	retval = true;
    }

    if (!this->lex->get_token(token))
	retval = false;

    return retval;
}

void SyntaxAnalyzer::perform_operation(char oper)
{
    if (!this->parseOnly) {
	double num1, num2, result;
	
	num1 = this->stack.back();

        this->stack.pop_back();
        num2 = this->stack.back();
        this->stack.pop_back();

	cout << "    Evaluating expression: "
	    << num2 << " " << oper << " " << num1
	    << endl;
    
        if (oper == '+') {
    	    result = num2+num1;
        } else if (oper == '-') {
    	    result = num2-num1;
        } else if (oper == '*') {
    	    result = num2*num1;
        } else if (oper == '/') {
    	    result = num2/num1;
        }

	cout << "    Result: " << result << endl;
    
        this->stack.push_back(result);
    } else {
	cout << "    Skipping evaluation, by user request." << endl;
    }
}

void print_help(char* arg0)
{
    	cerr << "Evaluates expressions in a file." << endl;
	cerr << "To read from a file: " << arg0 << " -f <filename>" << endl;
	cerr << "To read from a file but only parse (not eval): " << arg0 << " -fp <filename>" << endl;
	cerr << "To evaluate interactively: " << arg0 << " -c" << endl;
	cerr << "To parse (not eval) interactively: " << arg0 << " -cp" << endl;
}

int runfile(SyntaxAnalyzer *syn, string filename)
{
    int retval = 0;
    ifstream inputFile;
    string buffer;

    inputFile.open(filename.c_str(), ios::in);
    if (inputFile.is_open()) {
	while (inputFile.good()) {
	    getline(inputFile, buffer);
	    cout << "Beginning evaluation of expression: " << buffer << endl;
	    syn->evaluate(buffer);
	}
	inputFile.close();
    } else {
	cerr << "Error: could not open file " << filename << " for reading." << endl;
	retval = 1;
    }

    return retval;
}

void runrepl(SyntaxAnalyzer *syn)
{
    string buffer;

    cout << "Type in the expression you want to evaluate.  Type 'quit' to end." << endl;
    bool flag = false;
    while (!flag) {
	cout << "expr: ";
	getline(cin, buffer);
	if (buffer == "quit") {
	    flag = true;
	} else {
	    syn->evaluate(buffer);
	}
    }
}



int main(int argc, char *argv[])
{
    	int retval = 0;
	ifstream inputFile;
	string buffer;

	SyntaxAnalyzer *syn = new SyntaxAnalyzer();

	if (argc > 1) {
	    string cmd = argv[1];
	    
	    if (cmd == "-f") {
		if (argc != 3) { // [program] -f [filename]
		    cerr << "Error: need a filename." << endl;
		    retval = 1;
		} else {
		    runfile(syn, argv[2]);
		}
	    } else if (cmd == "-c") {
		runrepl(syn);
	    } else if (cmd == "-fp") {
		if (argc != 3) {
		    cerr << "Error: need a filename." << endl;
		    retval = 1;
		} else {
		    syn->parseOnly = true;
		    runfile(syn, argv[2]);
		}
	    } else if (cmd == "-cp") {
		syn->parseOnly = true;
		runrepl(syn);
	    } else {
		print_help(argv[0]);
		retval = 1;
	    }
	} else {
	    print_help(argv[0]);
    	    retval = 1;
	}

	return retval;
}

