/* Capitol College CS512
 * James Curbo <jcurbo@pobox.com>
 *
 * Project - Part 1
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>


/* Converts a string containing a natural number (non-negative integer) to an integer value.
 * Returns the value, or -1 if a non-digit character was encountered in the
 * string.
 */
int convert(char *s) {
	int value = 0;

	for(int i = 0; i < strlen(s); i++) {
		if (!isdigit(s[i])) {
			/* choke */
			value = -1;
			break;
		} else {
			if (isdigit(s[i])) {
				int place = strlen(s) - i -1;
				value += (pow(10, place) * ((int)(s[i]) - '0'));
			}
		}
	}
	return value;
}



int main (int argc, char *argv[]) {
	if (argc > 1) {
		for (int count = 1; count < argc; count++) {
			printf("string %s, integer %d\n", argv[count], convert(argv[count]));
		}
	} else {
		printf("Converts strings that contain natural numbers to integers.\n");
		printf("Usage: natural <string> <string> ... <string>\n");
	}

	return 0;

}
