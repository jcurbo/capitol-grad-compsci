#include <iostream>
#include <vector>

using namespace std;

class Matrix
{
	public:
		Matrix(int x, int y);
		Matrix();

		float getElement(int row, int col);
		void setElement(int row, int col, float value);
		void print();
		Matrix add(Matrix a, Matrix b);
		int getHeight();
		int getWidth();
		void resize(int height, int width);

	private:
		int _height, _width;
		vector<vector<float> > _matrix;

};

Matrix::Matrix(int x, int y)
{
	_height = x;
	_width = y;
	this->resize(_height, _width);
}

float Matrix::getElement(int row, int col) 
{
	return _matrix[row][col];
}

void Matrix::setElement(int row, int col, float value)
{
	_matrix[row][col] = value;
}

void Matrix::print()
{
	vector<vector<float> >::iterator xi;
	vector<float>::iterator yi;

	for (xi = _matrix.begin(); xi != _matrix.end(); ++xi) {
		for (yi = (*xi).begin(); yi != (*xi).end(); ++yi) {
			cout << *yi;
		}
		cout << endl;
	}
}

int Matrix::getHeight()
{
	return _height;
}

int Matrix::getWidth()
{
	return _width;
}

void Matrix::resize(int height, int width)
{
    _matrix.resize(height);
    vector<vector<float> >::iterator it;
    for (it = _matrix.begin(); it != _matrix.end(); ++it)
	(*it).resize(width);
}

// On failure: returns an empty matrix. (check getHeight and getWidth == 0)
// On success: returns a new matrix with the added contents
Matrix Matrix::add(Matrix a, Matrix b)
{
    	Matrix result;
	int height = a.getHeight();
	int width = a.getWidth();

	if (height == b.getHeight() && width == b.getWidth()) {
	    result.resize(height, width);
	    for (int i=0; i<height; i++) {
		for (int j=0; j<width; j++) {
		    float sum = a.getElement(i,j) + b.getElement(i,j);
		    result.setElement(i, j, sum);
		}
	    }
	}
	return result;
}

