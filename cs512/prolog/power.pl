power(X,0,ResultX) :- ResultX is 1.

power(X,E,ResultX) :-
    Y is E-1,
    power(X,Y,ResultY),
    ResultX is X * ResultY.

isgreater(X,M) :- X > M.


testloop(0).
testloop(N) :-
    N > 0,
    write('num: '),
    write(N),
    nl,
    M is N-1,
    testloop(M).

addloop(S,0).
addloop(S,I) :-
    I>0,
    %S is S+I,
    write('result: '),
    write(S),
    nl,
    J is I-1,
    T is S+I,
    addloop(T,J).

addloop2(S,1,Result) :- Result is S+1.
addloop2(S,I,ResultS) :-
    I>0,
    J is I - 1,
    T is S + I,
    addloop2(T,J,ResultT),
    ResultS is ResultT.
