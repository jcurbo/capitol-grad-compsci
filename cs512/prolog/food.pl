people(john).
people(sue).
people(tom).

% Apples / oysters are food
food(apples).
food(oysters).
% Anything anyone eats is food
food(Y) :- eats(people, Y).

% Tom eats snakes
eats(tom, snakes).

% John eats all kinds of food
eats(john, X) :- food(X).

% Sue eats everything Tom eats
eats(sue, X) :- eats(tom, X).


%%%
botheat(X, Y) :- eats(X,Z), eats(Y,Z).
