{- memberSet -}

memberSet :: (Eq a) => a -> [a] -> Bool
memberSet x [] = False
memberSet x (s:s2)
	| x == s = True
	| otherwise = memberSet x s2

stuff = [1,2,3,4]
res = memberSet 0 stuff
	
{- subSet -}

subSet :: (Eq a) => [a] -> [a] -> Bool
subSet [] _ = True
subSet (s:s1) s2
	| memberSet s s2 = subSet s1 s2
	| otherwise = False

set1 = [1,2,3,4]
set2 = [1,2,3]
set3 = [5,6,7]
res2 = subSet set2 set1
res3 = subSet set3 set1

{- cardSet -}
cardSet :: [a] -> Integer
cardSet [] = 0
cardSet (s:s1) = 1 + cardSet s1

set4 = [1,2,3,4]
res4 = cardSet set4

{- addSet -}

addSet :: (Eq a) => a -> [a] -> [a]
addSet x s
	| memberSet x s = s
	| otherwise = x:s

set5 = [1,2,3]
item = 4
item2 = 3
set6 = addSet item set5
set7 = addSet item2 set5

{- removeSet -}

removeSet :: (Eq a) => a -> [a] -> [a]
removeSet x (s:s1)
	| not (memberSet x (s:s1)) = s:s1
	| x == s = s1
	| otherwise = removeSet x s1++[s]

set8 = [1,2,3]
set9 = removeSet 1 set8
set10 = removeSet 0 set8
	
main = do
	print "memberSet, should be False - "
	print res
	print "subSet 1, should be True - "
	print res2
	print "subSet 2, should be False - "
	print res3
	print "cardSet, should be 4 - "
	print res4
	print "addSet, should be [4,1,2,3] - "
	print set6
	print "addSet, should be [1,2,3] - "
	print set7
	print "removeSet, should be [2,3] - "
	print set9
	print "removeSet, should be [1,2,3] - "
	print set10


