(ns newproj.core)

(defn power [a n]
  (let [multiply (fn [x factor i]
                   (if (zero? i)
                     x
                     (recur (* x factor) factor (dec i))))]
    (multiply a a (dec n))))

(defn mypow1 [a n]
  (if (zero? n)
    1
    (* a (mypow1 a (dec n)))))

(defn mypow2 [a n]
  (loop [z 1 n n]
    (if (zero? n) z
      (recur (* a z) (dec n)))))

(defn mypow-lazy [a n]
  (if (zero? n)
    1
    (nth (iterate (partial * a) a) (dec n))))
