#!/usr/bin/python

import sys

import fileinput

s = ""
for line in fileinput.input():
    s += line

#s = "abcdefghijklmnopqrstuvwxyz"
#s = "zgo vav a twugew kmuuwkkxmd kdgodq sl xajkl lzwf sdd gx s kmvvwf - sfgfqegmk"
#s = "f dta dta rmkn nxonqfnlen fl hjteihfqaflc hnbmqn f wnls onqjflc fl sdn otukmsur. msmm tla f wnqn ml sdn hnted fl rtkmt wn qntjjy wnqn ml sdn hnted tla dtqa tcqmula-wdnl ky edtlen etkn sm cm tr qnequfsnq ml t hjteihfqa hqfc. msmm rfclna ml hnbmqn sdn ktrs; tla bmq sdn lnxs dtjb amznl yntqr, fl tr ktly rdfor, wn ilmeina thmus sdn wfjanrs omqsfmlr mb knjtlnrft -sdn dntsdnl hy gtei jmlaml"

i = 0
while i<26:
    # print our current iteration and conversion of 'a' so we know where we
    # are in the table
    print 'iteration %i, a = %s' % (i, chr(ord('a')+i))
    # this loop iterates over the entire string
    for c in s.lower():
        # If outside the range a-z (using ascii values), just print char and
        # skip rest of loop
        if ord(c) < ord('a') or ord(c) > ord('z'):
            sys.stdout.write(c)
            continue
        # shift char by 'i' places, use modulo to keep value within a-z range
        # we must add the ascii value of 'a' back to the value at the end to
        # shift the value back into the ascii range a-z
        sys.stdout.write( ( chr( ((ord(c) - ord('a')) + i ) % 26 + ord('a')) ) )
    print
    i += 1


