#include <stdio.h>
#include <string.h>

int main(void) {
    char * s = "abcd";

    int count = 0;
    int i;
    while (count<26) {
	for (i=0; i < strlen(s); i++) {
		if (s[i] < 'a' || s[i] > 'z') {
		    putchar(s[i]);
		    continue;
		}
		putchar((s[i] + count) % 26);
		}
	putchar('\n');
	count++;
    }
    return 0;
}
