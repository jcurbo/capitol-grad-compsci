#!/usr/bin/python

# This script does a simple frequency analysis of text passed to it via stdin
# or filenames.  It currently ignores case.

import fileinput
from string import ascii_lowercase

data = ""

# pull all text into one variable
for line in fileinput.input():
    data += line

# this dict holds the counts.  keys are lowercase letters a-z
chardata = dict()

for c in ascii_lowercase:
    chardata[c] = data.lower().count(c)

keylist = chardata.keys()
keylist.sort()
for i in keylist:
    print "%s %-3d" % (i, chardata[i])



