#!/usr/bin/python

import fileinput

data = ""

for line in fileinput.input():
    data += line

data1 = ""
data2 = ""

data1 = data[0:None:2]
data2 = data[1:None:2]

f1 = open('file1.txt', 'w')
f2 = open('file2.txt', 'w')

f1.write(data1)
f2.write(data2)

f1.close()
f2.close()
