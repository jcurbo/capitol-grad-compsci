#!/usr/bin/python

import fileinput
import re
import pprint

data = ""

# pull all text into one variable
for line in fileinput.input():
    data += line

# strips all but numbers and letters from data
#data_ch = ''.join(ch for ch in data if ch.isalnum())
data_ch = data

print data_ch

# dict for holding possible substrings
# key = length, value = list of substrings
data_sub = dict()

# for substring lengths between 2 and 10
for i in range(2,10):
    # create new list
    data_sub[i] = []
    # slice string from pos to pos + substr length, append to list, increment
    # pos by 1, repeat
    pos = 0
    while pos<len(data_ch)-i+1:
        data_sub[i].append(data_ch[pos:pos+i])
        pos+=1

# list of locations
# key = length of substring, value = dict where key = substring and value =
# locations (index in string)
locations = dict()

# iterates over the string, looking for substring matches
# saves the locations of the matches into the dictionary
for key in data_sub.keys():
    locations[key] = dict()
    for ss in data_sub[key]:
        list = [m.start() for m in re.finditer('(?=%s)' % ss, data_ch)]
        # we only want results where the substring occurs more than once
        if len(list) > 1:
            locations[key][ss] = list

pp = pprint.PrettyPrinter(indent=2)

#pp.pprint(locations)

#for key in data_sub.keys()
    #print "number of substrings of length %d with 2 or more occurences: %d" % (key, len(locations[key]))

#####################
# this chunk of code computes distances between each substring in the big
# string and saves it into a dict.  

# key = length, value = dict where key = substring, value = list of distances
# for that substring
distances = dict()

for key in locations.keys():
    distances[key] = dict()
    print "loc: working on keylength %d" % key
    for ss in locations[key]:
        print "  loc: working on substring %s" % ss
        distances[key][ss] = []
        pos = 0
        while pos<len(locations[key][ss])-1:
            # take the current item and the next item, subtract them and store
            index1 = locations[key][ss][pos]
            index2 = locations[key][ss][pos+1]
            print "    loc: working on list items %d and %d" % (index1, index2)

            dist = abs(index1-index2)
            distances[key][ss].append(dist)
            pos+=1

            print "    loc: distance between occurances of substring '%s': %d" % (ss, abs(index1-index2))

#pp.pprint(distances)

# now for factoring fun
# we need to determine the greatest common factor of all distances for a
# possible substring length.

# first, a function to find the greatest common factor (divisor) of two
# integers.  This is pretty straightforward, Euclid's algorithm
def gcf(a, b):
    while (a<>b):
        if(a>b):
            a=a-b
        else:
            b=b-a
    return a

# now we will iterate over the list of distances, computing their common
# factors and saving a count in a dict

factorcount = dict()

for key in distances.keys():
    print "dist: working on keylength %d" % key
    list = []
    
    # merge all the distances within one key length possibility to one list
    for ss in distances[key]:
        print "  dist: working on substring %s" % ss
        list += distances[key][ss]

    pos = 0
    while pos<len(list)-1:
        mygcf = gcf(list[pos], list[pos+1])
        if mygcf not in factorcount:
            factorcount[mygcf] = 1
        else:
            factorcount[mygcf] += 1
        pos +=1

pp.pprint(factorcount)







