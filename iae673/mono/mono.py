#!/usr/bin/python

from string import ascii_lowercase

# Data is input via stdin (pipes)
# this pulls it all into one string
data = "f dta dta rmkn nxonqfnlen fl hjteihfqaflc hnbmqn f wnls onqjflc fl sdn otukmsur. msmm tla f wnqn ml sdn hnted fl rtkmt wn qntjjy wnqn ml sdn hnted tla dtqa tcqmula-wdnl ky edtlen etkn sm cm tr qnequfsnq ml t hjteihfqahqfc. msmm rfclna ml hnbmqn sdn ktrs; tla bmq sdn lnxs dtjb amznl yntqr ,fl tr ktly rdfor, wn ilmeina thmus sdn wfjanrs omqsfmlr mb knjtlnrft -sdndntsdnl hy gtei jmlaml"

# dict for holding results of counting characters
chardata = dict()
cipherdata = dict()

# 'ascii_lowercase' contains a-z
# This gives us a count per character
for c in ascii_lowercase:
    chardata[c] = data.count(c)

# Prints the count, sorted
keylist = chardata.keys()
keylist.sort()
for i in keylist:
    print i, chardata[i]

# Original data
s1 = list(data)
# modified data
s2 = list(data)

# build a dict that tracks the status of each letter
used = dict()
# 0 = unused, 1 = used
for c in ascii_lowercase:
    used[c] = 0

green = '\033[91m'
endc  = '\033[0m'

while 1:
    print "\n\nCiphertext:"
    print data
    print "\nCurrent plaintext:"
    print ''.join(s2)

    c1 = raw_input("Pick a letter: ")
    if c1 == 'exit':
        break

    if used[c1] == 1:
        print "That letter has already been used, please select another one."
        continue

    used[c1] = 1

    c2 = raw_input("Replace with? ")

    # ic is the index, c is the character
    # this loops over the list, replacing c1 with c2 in the modified list if it matches
    for ic, c in enumerate(data):
        if s1[ic] is c1:
           s2[ic] = ''.join([green,c2,endc])
           cipherdata[c1] = c2

keylist = cipherdata.keys()
keylist.sort()
for i in keylist:
    print i, cipherdata[i]



